use crate::token::Token;

#[derive(Debug)]
pub struct Parser<'a> {
    tokens: &'a [Token],
    current: usize,
}

impl<'a> Parser<'a> {
    pub fn new(tokens: &'a [Token]) -> Self {
        Self { tokens, current: 0 }
    }

    fn current_token(&self) -> Option<&Token> {
        self.tokens.get(self.current)
    }

    fn advance(&mut self) {
        self.current += 1;
    }

    pub fn parse_program(&mut self) -> Result<Vec<String>, String> {
        let mut statements = Vec::new();
        while self.current_token().is_some() {
            // Simple placeholder parsing logic
            if let Some(token) = self.current_token() {
                statements.push(format!("Parsed token: {:?}", token));
                self.advance();
            }
        }
        //println!("{:?}", statements);
        Ok(statements)
    }
}

